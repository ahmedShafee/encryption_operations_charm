from functools import reduce

from charm.toolbox.pairinggroup import *

from charm.toolbox.secretutil import SecretUtil

from charm.toolbox.ABEncMultiAuth import ABEncMultiAuth

import re

import time

import random

#from charm.core.math.integer import *

from charm.toolbox.pairinggroup import *

from charm.toolbox.secretutil import SecretUtil

from charm.toolbox.ABEncMultiAuth import ABEncMultiAuth

from random import randint

from charm.core.math.integer import *

from decimal import Decimal






def mul(x, y): return x*y


def my_reduce(seq):

	first = seq[0]

	for i in seq[1:]:

		first = mul(first, i)

	return first



def chinese_remainder(a, m):

""" return x in ' x = a mod m'.

"""

	enc2 = []
	modulus = integer(5)
	modulus = my_reduce(m)
	M = integer(5)
	m_i = integer(5)
	multipliers = []
	no_of_experiments3 = range(30)


	for m_i in m:

		M = (modulus / m_i) % modulus
		inverse = ((integer(1) % m_i) / (M % m_i)) % modulus
		multipliers.append(inverse * M % modulus)
		result = integer(0)% modulus

	t2 = time.time()

	for multi, a_i in zip(multipliers, a):
		result = (result + multi * (a_i % modulus)) % modulus
	t3 = time.time()-t0

		
	return t3, result



def generate_big_prime(n,q):

	found_prime = False

	while not found_prime or p in q:

		p = randomPrime(n)

		found_prime = isPrime(p)

	return p



if __name__ == '__main__':

	q = []

	p = []
	
	group = PairingGroup('SS512')

	enc = []

	enc2 = []

	l2 = []

	x = integer(5)

	temp = integer(5)

	temp2 = integer(5)


	no_of_experiments = range(1)

	no_of_experiments2 = range(1)

	for i in no_of_experiments:

		temp = integer(5)

	x = 1231231414123

	print('x=', x)

	for j in range(25):
		temp = generate_big_prime(20,q)
		temp2 = x % temp
		q.append(temp)
		p.append(temp2)
		print(q)

	t0 = time.time()

	for k in no_of_experiments2:

		t3, CR = chinese_remainder(p, q)

		l2.append(t3)

		print('CR=', int(CR))

		avgl = sum(l2)/len(l2)

	t1 = time.time()-t0

	enc += t1/len(no_of_experiments2)

	enc2 += avgl
